# -*- coding: utf-8 -*-
"""
=================================================================
Copyright 2018 James Campbell & Jeroen Hubert

 This file is part of Animal Tracker.

    Animal Tracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Animal Tracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Animal Tracker.  If not, see <https://www.gnu.org/licenses/>. 
=================================================================

This file is a modification of the unlicensed code presented in the following
 tutorial:
https://www.pyimagesearch.com/2017/02/06/faster-video-file-fps-with-cv2-videocapture-and-opencv/
"""

# Load libraries
from threading import Thread, Lock
import time
from queue import Queue
from PyQt5.QtCore import QObject, pyqtSignal
import sys
import cv2

class VideoStream(QObject):
    
    # For communicating betwee thread and UI buffer priogress bar
    progress_update = pyqtSignal(int)
        
    def __init__(self, queue=20):
        # Queue size determines the number of frames thta can be buffered

        super().__init__()

        print("Initializing Video Streamer")

        """
        This Lock object can be used to pause the thread while settings
        are changed.

        acquire() 'Aquire' the lock.  If another thread has already aquired 
            the lock, then it will pause until it is released and it can then 
            aquire it for itself
        release() release the lock, so other threads can "aquire" it

        The gist is.. when acessing or changing shared variables, use aquire 
        and lock to make sure the values are not changed simutaniously accross 
        multiple threads

        These methods are intrisnicly implemented in the Queue function
        """

        self.frame_last = 0

        self.lock = Lock()
        # The queuing object for threading
        # Will continously fill a buffer of <queue> frames
        self.Q = Queue(maxsize=queue)

        self.videoLoaded = False

        # Make new thread and run it
        self.t = Thread(target=self.buffer, args=())
        self.t.daemon = True
        self.t.start()

    def __del__(self):
        # Deconstructor:  Will wait until thread is terminated
        # before closing object
        sys.exit()
        print("video - Video Streamer Closed")

    def streamOpen(self):
        return self.videoLoaded

    def loadVideo(self, path):

        self.lock.acquire()
        self.stream = cv2.VideoCapture(path)
        self.lock.release()

        if self.stream.isOpened():
            self.stopped = False  # For stopping the stream

            self.lock.acquire()
            # Last frame read
            self.frame_last = 0
            # Save video properties
            self.frames = self.stream.get(cv2.CAP_PROP_FRAME_COUNT)
            self.width = self.stream.get(cv2.CAP_PROP_FRAME_WIDTH)
            self.height = self.stream.get(cv2.CAP_PROP_FRAME_HEIGHT)
            self.fps = self.stream.get(cv2.CAP_PROP_FPS)
            self.lock.release()

            self.setVideoParams()

            print("video - Frames in Video: " + str(self.frames))
            # Start streaming for first frame
            self.lock.acquire()
            self.videoLoaded = True
            self.lock.release()

            return(True)
        else:
            return(False)

    def setVideoParams(self, radius=5, sd=1, scale = 1):

        self.lock.acquire()
        # Update blur settings
        self.blur_radius = radius
        self.blur_sd = sd
        self.scaleVideo = scale
        self.lock.release()

    def getFrame(self, frameNumber):

        #print("video - Frame Last: " + str(self.frame_last) + ", Frame: " +
#              str(frameNumber))
        if frameNumber != (self.frame_last + 1):
            print("video - Refresh buffer")
            # Restart buffer if frame to grab is not next in the queue
            self.restart(frameNumber)
            self.frame_last = frameNumber - 1

    def read(self):
        # Load frame and increment frame counter
        # print("video - FrameLast: " + str(self.frame_last) + 
        #       " frames Total: " + str(self.frames))
        if self.Q.empty():
            print( "video - Queue empty...")
            if self.frame_last >= (self.frames - 2):
                print("video - End of video")
                return(False, [])
            else:
                print("video - waiting for buffer to refresh")
                while self.Q.empty():
                    time.sleep(0.1)
                    self.frame_last = self.frame_last + 1
                return(True, self.Q.get())
        else:
            self.frame_last = self.frame_last + 1
            return(True, self.Q.get())

    def restart(self, frameNumber):
        # Restart buffer when jumpin frames non-sequentially

        # Pause thread
        self.lock.acquire()
        # Set new frame value
        self.stream.set(cv2.CAP_PROP_POS_FRAMES, frameNumber - 1)
        print("video - New start frame: "
              + str(self.stream.get(cv2.CAP_PROP_POS_FRAMES)))
        # empty Queue
        self.Q.queue.clear()
        print("video - Buffer size: " + str(self.Q.qsize()))
        self.frame_last = frameNumber - 1
        # Resume thread
        self.lock.release()

    def bufferSize(self):
        # returns current buffer size.
        # -1 indicates that the buffer is not running
        return self.Q.qsize()

    def buffer(self):
        # buffer refresh function
        """
        This will keep on looping and try to fill the queue when room is
        available.
        The queue is a buffer of frames.  When a frame is read its popped from
        the queue, and when an additional spot is opened, this function will
        fill it with another frame.  This takes place on a seperate thread
        so it doesnt slow down the UI.

        Note that this buffer will have to be reset eachtime the user jumps
        more than 1 frame forward, as this is based on sequentially loading
        the frames in chronological order.
        """
        # Infinite loop
        while True:
            #  If queue not full
            if not self.Q.full():

                # Lock while grabbing frame
                self.lock.acquire()
                loaded = self.videoLoaded
                self.lock.release()

                if loaded:
#                    print("video - Buffer: " + str(self.Q.qsize()))
                    self.lock.acquire()
                    #print("stream frame: " + str(self.stream.get(cv2.CAP_PROP_POS_FRAMES)))
                    # grab next frame
                    ret, frame = self.stream.read()
                    if ret:
                        # resize frame
                        frame = cv2.resize(frame, (int(self.width*self.scaleVideo),
                                                   int(self.height*self.scaleVideo)))
                        # Apply Guassian Blur to frame
                        if self.blur_radius > 0:
                                frame = cv2.GaussianBlur(frame,
                                                         (int(self.blur_radius),
                                                          int(self.blur_radius)),
                                                          self.blur_sd)
                        # Add frame to Queue
                        self.Q.put(frame)
                    else:
                        # Likley end of video.  Wait for user to select new frame
                        time.sleep(0.1)
                    self.progress_update.emit(100 * self.Q.qsize() / self.Q.maxsize)
                    self.lock.release()
            else:
                time.sleep(0.1)
