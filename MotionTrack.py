#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=================================================================
Copyright 2018 James Campbell & Jeroen Hubert

 This file is part of Animal Tracker.

    Animal Tracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Animal Tracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Animal Tracker.  If not, see <https://www.gnu.org/licenses/>. 
=================================================================
Created on Mon Nov  5 18:59:49 2018

@author: jac
"""
import cv2
import numpy as np
import time
import VideoStream
from PyQt5 import QtCore
from PyQt5.QtGui import QPixmap, QImage  # Displaying images in UI
# Threading
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
from threading import Thread, Lock  
from queue import Queue
import sys
"""
This class will take raw video frames as input from the VideoStream object and
output the painted frame (painted with tracking results), centroid of object,
and largest object for each arena defined
"""


class Tracker(QObject):

    progress_update = pyqtSignal(int)
    signal_maxContour = pyqtSignal(str)  # Update largest contour text

    # Tracking session
    centroids = None  # Should be a dataframe of length [frames]
    file_tracking = None  # Path to tracking file
    file_arena = None  # path to arena file

    # MOG settings
    mog = None  # Object holding mog2 object
    mog_history = None
    mog_Thresh = None
    mog_detectShadows = False
    frameNumber = 1

    def __init__(self, queue=20):
        # Queue size determines the number of frames thta can be buffered
        
        super().__init__()  # invoke parent class constructor.
        # this is necessary to initalize our signal/slot mechanism
        # see https://stackoverflow.com/questions/2970312/pyqt4-qtcore-
        #pyqtsignal-object-has-no-attribute-connect

        print("Initializing Tracker")

        # Last frame read
        self.frame_last = 0
        self.videoLoaded = False

        # Init buffered video streamer
        self.stream = VideoStream.VideoStream(queue)
        # Init lock
        self.trackingParams_lock = Lock()
        # Init default tracking parameters
        self.setTrackerParams()
        self.setVideoParams()
        
        self.setArenas()
        self.setArenaMode(False)

        self.frame_start = 1
        self.frame_end = 100

        # The queuing object for threading
        # Will continously fill a buffer of <queue> frames
        self.Q = Queue(maxsize=queue)

        self.currentFrame = 0

        # Make new thread and run it
        self.t = Thread(target=self.buffer, args=())
        self.t.daemon = True
        self.t.start()

    def __del__(self):
        # Deconstructor:  Will wait until thread is terminated
        # before closing object
        sys.exit()
        print("Tracker Closed")

    def setArenas(self, arenas=[]):
        # Order arenas by left-most first pixel
        self.trackingParams_lock.acquire()
        self.arenas = arenas
        self.trackingParams_lock.release()

    def loadVideo(self, path):
        ret = self.stream.loadVideo(path)
        # Set video size (scaled)

        if ret:
            self.trackingParams_lock.acquire()
            self.frame_width = int(self.stream.stream.get(cv2.CAP_PROP_FRAME_WIDTH) * 
                                self.scaleVideo)
            self.frame_height = int(self.stream.stream.get(cv2.CAP_PROP_FRAME_HEIGHT) * 
                                    self.scaleVideo)
            self.frames = int(self.stream.stream.get(cv2.CAP_PROP_FRAME_COUNT))
            self.trackingParams_lock.release()
            self.restart(self.frame_start)

        self.videoLoaded = ret
        self.setVideoParams()
        return(ret)

    def restart(self, frameNumber):
        # Restart buffer when jumping frames non-sequentially
        # Set new frame value
        self.trackingParams_lock.acquire()
        self.frame_last = frameNumber - 1
        self.stream.restart(frameNumber)
        print("motion - New start frame: " + str(frameNumber))
        # empty Queue
        self.Q.queue.clear()
        print("motion - Buffer size: " + str(self.Q.qsize()))
        self.trackingParams_lock.release()
        
    def getFrame(self, frameNumber):
        frameNumber = max(frameNumber, 1)
        # Restart buffer if skipping frames
        if frameNumber != (self.frame_last + 1):
            # Restart tracking buffer if frame to grab is not next in the queue
            self.restart(frameNumber)
            # Restart video buffer if frame to grab is not next in the queue
 
        # Return next frame
        self.frameNumber = frameNumber
        return self.read()

    def read(self):
        # Load frame and increment frame counter
        # Grab next frame in buffer
        print("motion - Frame Last: " + str(self.frame_last) + ", Frame: " +
            str(self.frameNumber) + "/" + str(self.frames))
        if self.Q.empty():
            print( "motion - Queue empty...")
            if self.frame_last >= (self.frames - 2):
                print("motion - End of video")
                return(False, None)
            else:
                while self.Q.empty():
                    print("motion - waiting for buffer to refresh")
                    time.sleep(0.1)
                self.frame_last = self.frame_last + 1
                return(True, self.Q.get())
        else:
            self.frame_last = self.frame_last + 1
            return(True, self.Q.get())

    def paintFrame(self, frame, contours_max, contours_all, centroids, bin):
        ###################################
        # Paint graphics on tracked frame #
        ###################################
        if len(self.arenas) > 0:
            for a in self.arenas:
                # Draw polygon for each arena
                pts = np.int32(np.asarray(a))
                # open cv only accepts 32-bit arrays
                cv2.polylines(frame,
                              [pts],
                              color=(0, 0, 200),
                              isClosed=True,
                              thickness=5)

            if contours_all:
                cv2.drawContours(frame, contours_all, -1,
                                 (150, 0, 0), -1)
            if contours_max:
                cv2.drawContours(frame, contours_max, -1,
                                 (200, 200, 0), -1)
            if centroids:
                for centroid in centroids:
                    cv2.circle(frame, centroid, 5, (0, 255, 0), 2)

        # Convert to QPixmap and return
        return(frame)

    def cvToQPix(self, cvImg):
        # This converts opencv read frames into QPixmap images so they can be
        # displayed in pyqt
        colorChannels = 3
        height = len(cvImg)
        width = len(cvImg[0])
        bytesPerLine = colorChannels * width
        # Make Qimage
        qImg = QImage(cvImg, width, height, bytesPerLine,
                      QImage.Format_RGB888)
        # Convert to QPixmap
        pixmap = QPixmap.fromImage(qImg)
        # Return Pixmap
        return pixmap

    def getTrackerParams(self):
        # Load user params for display in the UI
        return([self.mog_history, self.mog_Thresh, self.bin_lower_thrh,
                self.bin_high_thrh, self.min_cont_surface_subject,
                self.imageCleaning, self.erosion, self.erosion_it,
                self.dilation, self.dilation_it])

    def getVideoParams(self):
        return([self.blur_kernel, self.blur_sd, self.scaleVideo])

    def setArenaMode(self, arenaMode):
        self.arenaMode = arenaMode

    def setVideoParams(self, kernel=5, sd=1, scaleVideo=1):
        self.blur_kernel = kernel
        self.blur_sd = sd
        self.scaleVideo = scaleVideo

        # Apply video params to videostreamer
        self.stream.setVideoParams(kernel, sd, scaleVideo)

        self.blur_radius = kernel
        self.blur_sd = sd
        self.scaleVideo = scaleVideo

        if self.videoLoaded:
            # Reset buffer with new values
            self.getFrame(self.frame_last)

            self.trackingParams_lock.acquire()
            # Set video size (scaled)
            self.frame_width = int(self.stream.stream.get(cv2.CAP_PROP_FRAME_WIDTH) * 
                                   self.scaleVideo)
            self.frame_height = int(self.stream.stream.get(cv2.CAP_PROP_FRAME_HEIGHT) * 
                                    self.scaleVideo)
            self.trackingParams_lock.release()

        # Reset video paraeters to update frame
        params = self.getTrackerParams()
        self.setTrackerParams(params[0], params[1], params[2], params[3], 
                              params[4], params[5], params[6], params[7],
                              params[8], params[9])
        
    def setTrackerParams(self, history=85, threshold=25,
                  bin_thresh_low=200, bin_thresh_high=255,
                  min_cont_surface_subject=10, imageCleaning=0,
                  erosion=5, erosion_it=1, dilation=5, dilation_it=1):

        self.trackingParams_lock.acquire()
        self.mog_history = history
        self.mog_Thresh = threshold
        self.bin_lower_thrh = bin_thresh_low
        self.bin_high_thrh = bin_thresh_high
        self.min_cont_surface_subject = min_cont_surface_subject
        self.imageCleaning = imageCleaning
        self.erosion = erosion
        self.erosion_it = erosion_it
        self.dilation = dilation
        self.dilation_it = dilation_it

        print("Setting Tracking Params: " + 
            str([self.mog_history,
                self.mog_Thresh,
                self.bin_lower_thrh,
                self.bin_high_thrh,
                self.min_cont_surface_subject,
                self.imageCleaning,
                self.erosion,
                self.erosion_it,
                self.dilation,
                self.dilation_it]))


        # Create MOG2 object
        self.mog = cv2.createBackgroundSubtractorMOG2(int(self.mog_history),
                                                  self.mog_Thresh,
                                                  detectShadows = False)
        self.trackingParams_lock.release()

    def trackFrame(self, frame):
        # Uses the frame and arenas stored in class if not given
        # Convert to bw
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Loop through arenas
        rng = range(1, len(self.arenas)+1)
        
        ####################
        # Motion detection #
        ####################
        # Apply MOG2
        fgmask = self.mog.apply(frame)
        # Make MOG2 output binary
        ret, fgmask = cv2.threshold(fgmask,
                                    self.bin_lower_thrh,
                                    self.bin_high_thrh,
                                    cv2.THRESH_BINARY)
        # Loop through self.arenas
        binaries = []  # initalize empty list of binary arrays from each arena
        contours_max = []
        contours_all = []
        centroids = []
        maxArea = " "
        print(str(rng))
        for i in rng:
            ###############################
            # Apply mask to binary image  #
            ###############################
            # Make black mask
            mask = np.zeros(fgmask.shape, np.uint8)
            # Draw tracking arena on mask
            pts = np.int32(np.asarray(self.arenas[i-1]))
            cv2.fillConvexPoly(mask, pts, color=(255))
            # Apply mask to frame
            bw = mask * fgmask
            # Apply open/close binary processing
            if self.imageCleaning == 1:
                # Open
                bw = cv2.erode(bw,
                                cv2.getStructuringElement(cv2.MORPH_ELLIPSE,
                                                          (self.erosion, self.erosion)),
                                self.erosion_it)
                bw = cv2.dilate(bw,
                                cv2.getStructuringElement(cv2.MORPH_ELLIPSE,
                                                          (self.dilation, self.dilation)),
                                self.dilation_it)
            elif self.imageCleaning == 2:
                # Open
                bw =	 cv2.dilate(bw,
                                cv2.getStructuringElement(cv2.MORPH_ELLIPSE,
                                                          (self.dilation, self.dilation)),
                                self.dilation_it)
                bw =	 cv2.erode(bw,
                                cv2.getStructuringElement(cv2.MORPH_ELLIPSE,
                                                          (self.erosion, self.erosion)),
                                self.erosion_it)

            # Add arena tracking binary to list
            binaries.append(bw)
            # List contiguous groups of pixels
            _, contours, _ = cv2.findContours(bw,
                                              cv2.RETR_LIST,
                                              cv2.CHAIN_APPROX_NONE)
            # save contours in array
            contours_all = contours_all + contours

            centroid = (0, 0)
            cX = 0
            cY = 0
            if len(contours) > 0:
                # Find contour with largest area and save centroid
                contour = max(contours, key=cv2.contourArea)
                contours_max.append(contour)
                area = cv2.contourArea(contour)
                if area > self.min_cont_surface_subject:
                    M = cv2.moments(contour)
                    cX = int(M["m10"] / M["m00"])
                    cY = int(M["m01"] / M["m00"])
                    centroid = (cX, cY)
                else:
                    centroid = (0, 0)
                maxArea = maxArea + str(int(cv2.contourArea(contour))) + " "
            # Save contour and centroid
            centroids.append(centroid)

        # Merge binary images (from each arena) into a single image
        if len(binaries) > 0:
            bin = binaries[0]
            for i in range(1, len(binaries) - 1):
                bin = cv2.bitwise_or(bin, binaries[i])
        return([contours_max, contours_all, centroids, bin, maxArea])

    def buffer(self):
        """
        This will keep on looping and try to fill the queue when room is
        available.
        The queue is a buffer of frames.  When a frame is read its popped from
        the queue, and when an additional spot is opened, this function will
        fill it with another frame.  This takes place on a seperate thread
        so it doesnt slow down the UI.

        Note that this buffer will have to be reset eachtime the user jumps
        more than 1 frame forward, as this is based on sequentially loading
        the frames in chronological order.
        """
        # Infinite loop
        while True:
            #  If queue not full
            if not self.Q.full():
                if self.videoLoaded:
                    self.trackingParams_lock.acquire()
                    # grab next frame
#                    print("motion- Buffer: " + str(self.Q.qsize()))
                    ret, frame = self.stream.read()
                    if ret:
                        # Paint arenas
                        if self.arenaMode is True:
                            # Red outline in frame during define arena mode
                            cv2.rectangle(frame, (0, 0),
                                          (int(self.frame_width),
                                           int(self.frame_height)),
                                           thickness=5, color=(255, 0, 0))
                        if len(self.arenas) > 0:
                            # Process frame if arenas are present
                            contours_max, contours_all, centroids, bin, maxArea = self.trackFrame(frame)
                            frame_painted = self.paintFrame(frame, 
                                                            contours_max, 
                                                            contours_all, 
                                                            centroids, bin)
                            # Add frame to Queue
                            self.Q.put([self.cvToQPix(frame_painted),
                                        centroids, maxArea])
                        else:
                            self.Q.put([self.cvToQPix(frame), [], None])
                        self.progress_update.emit(100 * self.Q.qsize() /
                                                  self.Q.maxsize)
                    else:
                        # Likley end of video.  Wait for user to select new frame
                        time.sleep(0.1)
                    self.trackingParams_lock.release()
            else:
                time.sleep(0.1)
