# -*- coding: utf-8 -*-
"""
=================================================================
Copyright 2018 James Campbell & Jeroen Hubert

 This file is part of Animal Tracker.

    Animal Tracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Animal Tracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Animal Tracker.  If not, see <https://www.gnu.org/licenses/>. 
=================================================================
Created on Thu Nov  1 13:10:01 2018

@author: James Campbell

Title: Define arenas

This function will take a path to a video as its ar
"""
import numpy as np
# Reading folder contents
import os
# opencv, motion tracking functions
import cv2
# UI elements
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog, QInputDialog
# Signal/slots
from PyQt5.QtCore import pyqtSlot
import sys
import os.path

###############################################################################
# Launch UI                                                                  ##
###############################################################################
import mainwindow
import MotionTrack
# mainwindow is an automatically generated script from pyqt, do not edit the
# contents of that file!
# You can use qt creator to edit the associated .ui files and use the bash
# script qt-to-py.sh to convert crom a qt to python ui

# We're making our own QMainWindow subclass here where we can code in our
# own keybindings to the UI we made in qt creator.


class MyWindow(QtWidgets.QMainWindow):

    def __init__(self):

        self.arenaMode = False
        self.arena = []
        self.arenas = []
        self.playVideo = False
        self.updateUI = False

        # Need to call parent init to activate signal/slot mechanism
        super().__init__()

        # Start tracking object
        bufferSize = 20
        self.tracker = MotionTrack.Tracker(bufferSize)
        self.tracker.progress_update.connect(self.updateProgressBar_tracking)
        self.tracker.stream.progress_update.connect(self.updateProgressBar_video)
        self.batch = False

    def setBatch(self):
        self.batch = ui.checkbox_batch.isChecked()
        print("Batch mode: " + str(self.batch))

    def getFrameJump(self):
        i, okPressed = QInputDialog.getInt(self, "Jump to","Frame:",
                                           1, 0, self.frames, 1)
        if okPressed:
            return(True, int(i))
        else:
            return(False, 0)

    def updateProgressBar_tracking(self, maxVal):
        ui.pb_tracking.setValue(maxVal)

    def updateProgressBar_video(self, maxVal):
        ui.pb_buffer.setValue(maxVal)

    def defineArena(self):
        # Check that we are in arena definition mode
        if self.arenaMode is False:
            return
        # Get mouse position
        self.arena.append([int(self.mouse[0] * self.frame_width*self.scale),
                           int(self.mouse[1] * self.frame_height*self.scale)])

        print("Points: " + str(len(self.arena)))
        # Save new arena if complete (4 positions)
        if len(self.arena) >= 4:
            self.arenaMode = False
            self.arenas.append(self.arena)

            # Add new arena and sort according to left most position
            if len(self.arenas) > 1:
                xs = []
                for arena in self.arenas:
                    xs.append(arena[0][0])
                idx = sorted(range(len(xs)), key=lambda k: xs[k])
                self.arenas = [self.arenas[i] for i in idx]

            # tell tracker that arena is defined
            self.tracker.setArenas(self.arenas)
            self.tracker.setArenaMode(self.arenaMode)
            self.checkCentroidsArray()

            print("ArenaMode: " + str(self.arenaMode))
            # Reload frame (remove red box)
            self.loadFrame(self.currentFrame)
            self.saveArenas()

    def saveArenas(self):
        # save arena
        np.savetxt(self.abspath + "_arena.csv",
            np.asarray(self.arenas).flatten(), fmt="%1.1i", delimiter=",")

    def saveTrackingData(self):
        np.savetxt(self.abspath + "_centroids.csv",
            self.centroids.astype(int), fmt="%1.1i", delimiter=",")
        print("Tracking data saved")

    def checkCentroidsArray(self):
        extraColumns = (len(self.arenas) * 2) - len(self.centroids[0])
        print("Arenas: " + str(len(self.arenas)) +
         ", centroids: " + str(len(self.centroids[0])) +
         ", extra cols: " + str(extraColumns))

        rows = len(self.centroids)
        if extraColumns > 0:
            self.centroids = np.append(self.centroids, np.zeros([rows, extraColumns]), 1)
            print("Expanded centroid array to fit new arena")
            print("New dims: ", np.shape(self.centroids))
        else:
            print("Centroid array correct size")

    def loadTrackingData(self):
        if os.path.exists(self.abspath + "_centroids.csv"):
            self.centroids = np.genfromtxt(self.abspath + "_centroids.csv",
                delimiter=',')
        else:
            self.centroids = np.empty([int(self.frames), 2])
        # Get number of arenas saved in file
        self.checkCentroidsArray()
    def nextVideo(self):
        if self.batch:
            if ui.list_files.currentRow() < ui.list_files.count() - 1:
                idx = ui.list_files.currentRow()
                idx = min((idx + 1), (ui.list_files.count() - 1))
                ui.list_files.setCurrentRow(idx)
                self.loadVideo()
            else:
                self.playVideo = False
                print("No more videos in file list")

    def statusMessage(self, message):
        print("New statusbar message: " + message)

    def loadFrame(self, frame_number, show=True):
        # Check value is within video range
        frame_number = min(frame_number, self.frames - 3)
        frame_number = max(frame_number, 1)

        #print("Go to: " + str(frame_number))
        ret, trackResults = self.tracker.getFrame(frame_number)
        if ret:
            pixmap = trackResults[0]
            centroids = trackResults[1]
            # Save tracking results into array
            if len(centroids) > 0:
                #print("Saved: " + str(np.shape(self.centroids)) + ", new: ", str(np.shape(np.asarray(centroids).flatten())))
                self.centroids[int(frame_number)][0:(len(centroids)*2)] = np.asarray(centroids).flatten()
            #    print("Saved values: " + str(self.centroids[int(frame_number)]))
            # Show largest contour
            ui.etext_largestContour.setText(trackResults[2])

            # Load bitmap image into label
            ui.qlab_frame.setPixmap(pixmap)
            # Update current frame number
            self.currentFrame = frame_number

            # Update UI with new frame positon
            ui.etext_frame.setText(str(self.currentFrame) +
                                   " / " +
                                   str(self.frames))
            ui.etext_seconds.setText(str(round(float(self.currentFrame) /
                                               float(self.fps), 2)) +
                                     " / " +
                                     str(round(float(self.frames) /
                                               float(self.fps), 2)))
            cv2.waitKey(1)

    def mouseClick(self, event):
        # For grabbing mouse input while drawing arenas
        x = event.pos().x()
        y = event.pos().y()
        width = ui.qlab_frame.width()
        height = ui.qlab_frame.height()
        rel_x = float(x)/float(width)
        rel_y = float(y)/float(height)
        print("Mouse: " + str(round(rel_x, 2)) + ", " + str(round(rel_y, 2)))
        self.mouse = [rel_x, rel_y]
        self.defineArena()

    @pyqtSlot()
    def setTrackerParams(self):
        if self.updateUI == True:
            params = [int(ui.etext_history.text()),
                    int(float(ui.etext_distThresh.text())),
                    int(float(ui.etext_threshLow.text())),
                    int(float(ui.etext_threshHigh.text())),
                    int(float(ui.etext_minSize.text())),
                    int(ui.cbox_imageCleaning.currentIndex()),
                    int(float(ui.etext_erosion.text())),
                    int(float(ui.etext_erosion_it.text())),
                    int(float(ui.etext_dilation.text())),
                    int(float(ui.etext_dilation_it.text()))]

            self.tracker.setTrackerParams(params[0], params[1], params[2],
                                        params[3], params[4], params[5],
                                        params[6], params[7], params[8],
                                        params[9])
            # save to file
            np.savetxt(self.abspath + "_tracking.csv", np.asarray(params), fmt="%1.1i")
            # Reload frame to update tracking results
            self.loadFrame(self.currentFrame)

    def setVideoParams(self):
        # Round radius up to nearest odd number
        radius = int(float(ui.etext_blurSize.text()))

        if radius > 0:
            if (radius % 2) == 0:
                radius = radius + 1
        self.tracker.setVideoParams(radius,
                                    float(ui.etext_blurSD.text()),
                                    float(ui.etext_scaleVideo.text()))

        np.savetxt(self.abspath + "_video.csv",
                   np.asarray([float(ui.etext_frameStart.text()),
                               float(ui.etext_frameEnd.text()),
                               radius,
                               float(ui.etext_blurSD.text()),
                               float(ui.etext_scaleVideo.text())]),
                                fmt="%1.3f",)
        self.frame_start = float(ui.etext_frameStart.text())
        self.frame_end = float(ui.etext_frameEnd.text())
        self.loadFrame(self.currentFrame)

        self.scale = float(ui.etext_scaleVideo.text())

    def updateUI_TrackingParams(self, params):
        print("Setting Tracking UI: " + str(params))

        self.updateUI = False
        ui.etext_history.setText(str(params[0]))
        ui.etext_distThresh.setText(str(params[1]))
        ui.etext_threshLow.setText(str(params[2]))
        ui.etext_threshHigh.setText(str(params[3]))
        ui.etext_minSize.setText(str(params[4]))
        ui.cbox_imageCleaning.setCurrentIndex(int(params[5]))
        ui.etext_erosion.setText(str(params[6]))
        ui.etext_erosion_it.setText(str(params[7]))
        ui.etext_dilation.setText(str(params[8]))
        ui.etext_dilation_it.setText(str(params[9]))
        self.updateUI = True

    def updateUI_Frames(self, params):
        ui.etext_frameStart.setText(str(params[0]))
        ui.etext_frameEnd.setText(str(params[1]))

    def updateUI_VideoParams(self, params):
        ui.etext_frameStart.setText(str(self.frame_start))
        ui.etext_frameEnd.setText(str(self.frame_end))
        ui.etext_blurSize.setText(str(params[0]))
        ui.etext_blurSD.setText(str(params[1]))
        ui.etext_scaleVideo.setText(str(round(params[2], 2)))

    def refreshFiles(self):
        print("Refreshing file list")
        print(str(ui.list_files.currentRow()))
        print(str(ui.list_files.currentIndex()))
        filt = ui.etext_filefilter.text()
        path = ui.etext_workingdirectory.text()
        ui.list_files.setCurrentRow(-1)
        # Check that folder exists
        if os.path.exists(path):
            files = os.listdir(path)
            # Clear list
            ui.list_files.clear()
            # Add new files
            for file in files:
                # Check that suffix matches file filter (case insensitive)
                if file.upper().endswith(filt.upper()):
                    ui.list_files.addItem(file)
        else:
            print("Folder does not exist!")
    @pyqtSlot()
    def loadVideo(self, path=None):
        if ui.list_files.currentItem() is not None:
            if path is None:
                path = ui.list_files.currentItem().text()

            print("Loading video " + selected_dir + '/' + path)

            # Load video
            self.abspath = selected_dir + '/' + path
            videoLoaded = self.tracker.loadVideo(self.abspath)
        else:
            videoLoaded = False
            print("No video selected")


        # Check if opened correctly
        if videoLoaded is False:
            # If open failed
            ui.etext_currentvideo.setText("Error Reading")
            print("Error opening video stream or file")
            self.frames = 0  # Number of frames in currently loaded vid
            self.cap = None
            self.fps = None
            self.playVideo = False
            self.nextVideo() # go to next video if current is invalid
        else:
            # Set empty arenas
            self.arenas = []
            self.tracker.setArenas()
            # If open suceeded
            ui.etext_currentvideo.setText(path)
            print("New video loaded")
            # Set event callback functions for UI objects
            #ui.qlab_frame.mousePressEvent = self.mouseClick
            # Button callbacks
            ui.button_start.mouseReleaseEvent = self.play
            ui.button_stop.mouseReleaseEvent = self.stop
            ui.button_jumpFrame.mouseReleaseEvent = self.jumpToFrame
            ui.button_save.mouseReleaseEvent = self.save
            # Load video properties
            self.frames = self.tracker.stream.stream.get(cv2.CAP_PROP_FRAME_COUNT)
            self.fps = self.tracker.stream.stream.get(cv2.CAP_PROP_FPS)
            self.frame_width = self.tracker.stream.stream.get(cv2.CAP_PROP_FRAME_WIDTH)
            self.frame_height = self.tracker.stream.stream.get(cv2.CAP_PROP_FRAME_HEIGHT)
            # Set defaults
            self.frame_start = 1
            self.frame_end = self.frames
            # Load tracking settings from csv
            if os.path.exists(self.abspath + "_video.csv"):
                print("Loading Video params")
                video_array = np.genfromtxt(self.abspath + "_video.csv",
                                            delimiter=',')
                self.tracker.setVideoParams(video_array[2],
                                            float(video_array[3]),
                                            float(video_array[4]))
                self.frame_start = video_array[0]
                self.frame_end = video_array[1]
            if os.path.exists(self.abspath + "_tracking.csv"):
                print("Loading tracking params")
                tracking_array = np.genfromtxt(self.abspath + "_tracking.csv",
                                               delimiter=',')
                self.tracker.setTrackerParams(int(tracking_array[0]),
                    int(tracking_array[1]),
                    int(tracking_array[2]),
                    int(tracking_array[3]),
                    int(tracking_array[4]),
                    int(tracking_array[5]),
                    int(tracking_array[6]),
                    int(tracking_array[7]),
                    int(tracking_array[8]),
                    int(tracking_array[9]))

            if os.path.exists(self.abspath + "_arena.csv"):
                print("Loading arenas")
                arenas_flat = np.genfromtxt(self.abspath + "_arena.csv",
                                            delimiter=',')
                arenas_array = arenas_flat.reshape((int(len(arenas_flat)/8),
                                                    4, 2))
                self.arenas = arenas_array.tolist()
                # push arenas to tracker
                self.tracker.setArenas(self.arenas)
                # Reset buffer
                self.tracker.restart(self.frame_start)
            self.loadTrackingData()

            # Display tracking and batch params on UI
            self.updateUI_TrackingParams(self.tracker.getTrackerParams())
            self.updateUI_VideoParams(self.tracker.getVideoParams())
            self.scale = float(ui.etext_scaleVideo.text())
            self.updateUI_Frames([self.frame_start, self.frame_end])
            self.loadFrame(self.frame_start)
            if self.batch:
                self.play()

    def play(self, event=None):
        print('Start tracking')
        self.playVideo = True

        counter = 0

        while self.playVideo is True:
            counter = counter + 1
            if counter > 100:
                self.saveTrackingData()
                counter = 0
            self.loadFrame(self.currentFrame + 1, show=True)
            app.processEvents()  # necessary for refreshing UI
            if (self.currentFrame >= self.frames - 3) or (self.currentFrame >= self.frame_end):
                self.saveTrackingData()
                if self.batch:
                    self.nextVideo()
                else:
                    self.playVideo = False

    def stop(self, event=None):
        print("Stopping playback")
        self.playVideo = False

    def save(self, event=None):
        self.saveTrackingData()
        print("Data saved for this video")

    def jumpToFrame(self, event=None):
        ok, num = self.getFrameJump()
        if ok:
            self.loadFrame(num)
            print("Jumping to frame: " + str(num))

    @pyqtSlot()
    def keyPressEvent(self, event):
        key = event.key()
        print('Key: ' + str(key))
        # UI workspace controls
        if key is 79:
            # UI for choosing working directory
            print("Select Directory")
            path = str(QFileDialog.getExistingDirectory(self,
                                                        "Select Directory"))
            # Load new working directory in UI
            print(path)
            global selected_dir
            selected_dir = path
            print('seldir: ' + selected_dir)
            ui.etext_workingdirectory.setText(path)
            # Refresh file list
            self.refreshFiles()
        if key is 82:
            # Refresh file list
            self.refreshFiles()
        if key is 91:
            # Previous file
            idx = ui.list_files.currentRow()
            idx = max((idx - 1), 0)
            ui.list_files.setCurrentRow(idx)
            self.loadVideo()
        if key is 93:
            # Next file
            self.nextVideo()

        # UI video navigation #
        if key is 90:
            self.loadFrame(self.currentFrame - 1)
        if key is 88:
            self.loadFrame(self.currentFrame + 1)
        if key is 65:
            self.loadFrame(self.currentFrame - 10)
        if key is 83:
            self.loadFrame(self.currentFrame + 10)
        if key is 81:
            self.loadFrame(self.currentFrame - 100)
        if key is 87:
            self.loadFrame(self.currentFrame + 100)
        if key is 49:
            self.loadFrame(self.currentFrame - 1000)
        if key is 50:
            self.loadFrame(self.currentFrame + 1000)

        # UI Arena controls #
        if key is 43:  # Add new arena
            # Clear any previous partially defined arena
            self.arena = []
            self.arenaMode = True
            self.tracker.setArenaMode(self.arenaMode)
            print("ArenaMode: " + str(self.arenaMode))
            self.loadFrame(self.currentFrame)
        if key is 45:  # Remove previous arena
            self.arena = []
            self.arenaMode = False
            self.tracker.setArenaMode(self.arenaMode)
            print("Arenas: " + str(len(self.arenas)))
            if len(self.arenas) == 1:
                self.arenas = []
                self.tracker.setArenas(self.arenas)
                self.saveArenas()
                self.checkCentroidsArray()
                # Reload frame (remove red box)
                self.loadFrame(self.currentFrame)
            elif len(self.arenas) > 0:
                # Delete last arena
                self.tracker.trackingParams_lock.acquire()
                del self.arenas[len(self.arenas) - 1]
                self.tracker.trackingParams_lock.release()
                #self.tracker.setArenas(self.arenas)
                self.saveArenas()
                self.checkCentroidsArray()
                # Reload frame (remove red box)
                self.loadFrame(self.currentFrame)
            print("Arenas: " + str(len(self.arenas)))
        if key is 80:  # Start and preview Tracking
            self.play()
        if key is 92:
            self.stop()
        if key is 84:
            self.jumpToFrame()

# Start instance of UI
app = QtWidgets.QApplication(sys.argv)
# Create MainWindow widget
MainWindow = MyWindow()
# Load UI parameters
ui = mainwindow.Ui_MainWindow()
# Apply apply parameters to QMainWindow object
ui.setupUi(MainWindow)
######################################
# Connect signals to class functions #
######################################
# Update file list after filter change
ui.etext_filefilter.returnPressed.connect(MainWindow.refreshFiles)
# Update tracking parameters
ui.etext_distThresh.returnPressed.connect(MainWindow.setTrackerParams)
ui.etext_history.returnPressed.connect(MainWindow.setTrackerParams)
ui.etext_minSize.returnPressed.connect(MainWindow.setTrackerParams)
ui.etext_threshHigh.returnPressed.connect(MainWindow.setTrackerParams)
ui.etext_threshLow.returnPressed.connect(MainWindow.setTrackerParams)
ui.cbox_imageCleaning.currentIndexChanged.connect(MainWindow.setTrackerParams)
ui.etext_erosion.returnPressed.connect(MainWindow.setTrackerParams)
ui.etext_dilation.returnPressed.connect(MainWindow.setTrackerParams)
ui.etext_erosion_it .returnPressed.connect(MainWindow.setTrackerParams)
ui.etext_dilation_it.returnPressed.connect(MainWindow.setTrackerParams)
# update video parameters
ui.etext_frameStart.returnPressed.connect(MainWindow.setVideoParams)
ui.etext_frameEnd.returnPressed.connect(MainWindow.setVideoParams)
ui.etext_blurSD.returnPressed.connect(MainWindow.setVideoParams)
ui.etext_blurSize.returnPressed.connect(MainWindow.setVideoParams)
ui.etext_scaleVideo.returnPressed.connect(MainWindow.setVideoParams)
# Set Batch mode
ui.checkbox_batch.stateChanged.connect(MainWindow.setBatch)
# Load video on click
ui.list_files.currentItemChanged.connect(MainWindow.loadVideo)
# Enable mouse tracking on frame
ui.qlab_frame.setMouseTracking(True)
ui.qlab_frame.mousePressEvent = MainWindow.mouseClick
# Launch UI
MainWindow.show()
sys.exit(app.exec_())  # This runs a loop in the UI so it persists
