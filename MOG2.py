import cv2
import numpy as np
import pandas as pd

###########################################################
###  Start: ALL VARIABLES THAT CAN BE SET BY USER       ###
###########################################################
# Source file
VideoPath = './' # Folder with videos
#File = 'T-maze_short.mp4' # Video file File = 'T1.1Ambient6min30s.MTS' # Video file
# left or right maze
maze = 'right' # 'right' or 'left'

# Use a gray scale version of the video for tracking, this may reduce noise
greyscale = True

# Gaussian blurr (removes high frequency content (eg: noise, edges))
gaussian = True
gauss_kernel_x = 5 # Positive and odd number!! x-size of the kernel
gauss_kernel_y = 5 # Positive and odd number!! x-size of the kernel
gauss_kernel_sd = 1 # Standard deviation of x and y
# More info: https://docs.opencv.org/3.1.0/d4/d13/tutorial_py_filtering.html

# MOG2 settings
history = 85 # Length of the history (frames)
varThreshold = 25 # Threshold on the squared Mahalanobis distance between the pixel and the model to decide whether a pixel is well described by the background model
detectShadows = False # If true, the algorithm will detect shadows and mark them. It decreases the speed a bit.
# Source explaination: https://docs.opencv.org/3.4.3/de/de1/group__video__motion.html

# Make MOG2 output binary (black-white and not grayscale)
binary = True
bin_lower_thrh = 230 # lower threshold to make a grayscale pixel white
bin_high_thrh = 255 # upper threshold to make a grayscale pixel white, strongly advised to keep this 255!!

# Minimum detected surface of the subject
min_cont_surface_subject = 35

# Frame rate, only important for the time indication
frame_rate = 33.01

###########################################################
###  End: ALL VARIABLES THAT CAN BE SET BY USER         ###
###########################################################

# Make sure user can change dimension of frames
#cv2.namedWindow("original", cv2.WINDOW_NORMAL)
#cv2.namedWindow("fg", cv2.WINDOW_NORMAL)

# Define some functions for in the loop
cap = cv2.VideoCapture(VideoPath+File)
fgbg = cv2.createBackgroundSubtractorMOG2(history, varThreshold, detectShadows)

# Capture the first frame and determine the dimensions
ret, first_frame = cap.read()
height, width = first_frame.shape[:2]

# Create empty DataFrame for the tracked path
df = pd.DataFrame(columns=['File','Frame','Time','x','y'])

# Reset frame number
frame_no = 1

print("Press \'esc\' to exit")

# Loop that goes through all frames of the video
while True:
    # Read video
    ret, frame = cap.read()

    # Cover half of the video up
    if maze == 'right':
        frame_h = cv2.rectangle(frame,(0,0),(int(width * 0.5), height),(1),-1) # cover half of the video up with black
    if maze == 'left':
        frame_h = cv2.rectangle(frame,(int(width * 0.5),0),(width, height),(1),-1) # cover half of the video up with black

    # Sometimes the top of the video is a bit wacky, so let's cover it up with some black pixels
    frame_h = cv2.rectangle(frame,(0,0),(width, 8),(1),-1)

    # Make the video into gray scale if this parameter is set accordingly
    if greyscale == True:
        frame_h = cv2.cvtColor(frame_h, cv2.COLOR_BGR2GRAY)

    # Apply Gaussian blurr (removes high frequency content (eg: noise, edges))
    if gaussian == True:
        frame_h = cv2.GaussianBlur(frame_h, (gauss_kernel_x, gauss_kernel_y), gauss_kernel_sd)

    # Apply MOG2
    fgmask = fgbg.apply(frame_h)


    # Make MOG2 output binary
    if binary == True:
        ret, fgmask = cv2.threshold(fgmask,bin_lower_thrh,bin_high_thrh,cv2.THRESH_BINARY)
        bw = fgmask

    # Contour crab
    _, contours, _ = cv2.findContours(fgmask, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    if len(contours) != 0:
    # Minimum size for countour
        contour = max(contours, key = cv2.contourArea)
    #for contour in contours:
        area = cv2.contourArea(contour)
        if area > min_cont_surface_subject:
            cv2.drawContours(frame, contours, -1, (0, 255, 0), 4) # green
            # Calculate center of contour
            M = cv2.moments(contour)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            cv2.circle(frame, (cX, cY), 5, (0, 0, 255), -1) # red
        else:
            # If there's no contour that's big enhough, assume the animal stayed at the previous position
            cv2.circle(frame, (cX, cY), 5, (255, 255, 0), -1) # light blue

    # Resizing
    frame = cv2.resize(frame, (720, 405))
    fgmask = cv2.resize(fgmask, (720, 405))

    # Display the frames
    cv2.imshow('original', frame)
    cv2.imshow('fg', fgmask)

    # Inter-frame-interval
    k = cv2.waitKey(1) & 0xff # 33 is real time

    # Time indication
    #print("Frame number: %d Time: %d X-cord: %d Y-cord: %d" % (frame_no, frame_no/frame_rate, cX, cY))

    # Add data to df
    df.loc[frame_no] = [File, frame_no, round(frame_no/frame_rate,2), cX, cY]
    #print(df)

    frame_no = frame_no + 1

    # Stop the loop when the user presses 'esc'
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()

print(df)
df.to_csv(VideoPath+'tracking.csv', sep=',')
