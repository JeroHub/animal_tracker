\documentclass[11pt]{article}

%%%%%%%%%%%%%%%%% Skeleton %%%%%%%%%%%%%%%%%%
% Font settings
\usepackage[T1]{fontenc}
\usepackage{libertine}
\renewcommand*\ttdefault{lmvtt}
\usepackage{inconsolata}
% Margins
% Set to MSword style margins
\usepackage[margin=1in]{geometry}

% Section Formatting
\usepackage{titlesec}
\titleformat{\section}{\scshape\Large}{}{1em}{}[\titlerule]{}

%Caption formatting
\usepackage{caption}
\captionsetup[table]{skip=10pt}

% Images
\usepackage[dvipsnames,table]{xcolor}
\usepackage{graphicx}

% Figure labelling (inside figures)
\usepackage{stackengine}
\def\stackalignment{l}
% \topinset{label}{image}{from top}{from left}

% Textwrapped figures
\usepackage{wrapfig}
% \begin{wrapfigure}[numberLines]{placement}[overhang]{width}

% Tables
\usepackage{booktabs}
\usepackage{tabularx}
% Fix rowcolors for tabularx
\newcounter{tblerows}
\expandafter\let\csname c@tblerows\endcsname\rownum

% List settings
\usepackage{enumitem}
\setlist{itemsep=0em,parsep=0em}
%Change to alphabetical listing
%\begin{enumerate}[label=(\Alph*)]

% SI unit characters (includiong non-italic)
\usepackage{siunitx}
\usepackage{eurosym}
% Define Euro symbol
\DeclareSIUnit{\pers}{pers}
\DeclareSIUnit{\EUR}{\text{\euro}}
\sisetup{
  per-mode = symbol,
  inter-unit-product = \ensuremath{{}\cdot{}},
}
\DeclareSIUnit \amphour {Ah} %Define Amphour

% Code blocks
\usepackage{listings}
\definecolor{gitGrey}{rgb}{0.97,0.97,0.97}
\definecolor{gitRed}{rgb}{0.86,0.09,0.28}
\definecolor{gitGreen}{rgb}{0.07,0.60,.18}
\definecolor{ruleGrey}{rgb}{0.8,0.8,0.8}
\lstdefinestyle{github}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  backgroundcolor = \color{gitGrey},
  xleftmargin=0cm,
  extendedchars = true,
  showstringspaces=false,
  basicstyle=\ttfamily\small,
  keywordstyle=\ttfamily\small,
  commentstyle=\itshape\color{gitGreen},
  stringstyle=\color{gitRed},
  numbers=left,
  columns=fullflexible,
  tabsize=2}
  
% Hyper links
\usepackage[color links = true,
  all colors = blue]{hyperref}

% Hanging indents
\usepackage{hanging}
%%%%%%%%%%%%%% Draft settings %%%%%%%%%%%%%
% Line numbering
\usepackage[switch, modulo]{lineno}
%\linenumbers

% Enable highlighting
\usepackage{soul}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Flowchart
\usepackage{tikz}
\usetikzlibrary{shapes,arrows, positioning, fit, backgrounds}

\title{Animal Tracker}
\author{James Campbell, Jeroen Hubert}

\begin{document}
\maketitle

\DeclareRobustCommand{\keys}[1]{\colorbox{orange!50!yellow!30}{#1}}
\DeclareRobustCommand{\field}[1]{\colorbox{orange!50!red!30}{#1}}

\section{Overview}

Animal Tracker is an open source object tracking program made for analysing movement data in relatively controlled environments.
The program is built on the following frameworks:
\begin{itemize}
	\item python3
	\item pyQt 5
	\item openCV 3.4.3.
\end{itemize}

The intent of the program is to streamline the tracking process as much as possible.
Users can define their tracking arenas and settings ahead of time, then later batch process a folder of videos while leaving the program unattended.

Animal tracker uses the MOG2 background subtraction algorithm, thus Animal Tracker can deal with dynamic backgrounds and the user does not have to define a reference background image before tracking.
To get a quick overview of the processing pipeline Animal Tracker uses, see fig.~\ref{fig:flowchart}
\section{Quick Start}

Here we'll give a brief tutorial on how you can batch process videos with animal tracker.

\begin{enumerate}
	\item To run animal tracker, use the following command \lstinline[style=github]{python AnimalTracker.py}.
		So long as you have the openCV and python3 dependencies installed and correctly linked on your system, the program should launch.
	\item After launching, press \keys{O} to open a working folder and click on the \field{Files} tab to see the contents.
		This is the workspace for Animal Tracker and it can only batch process the contents of a single folder at a time.
		To filter out the contents of the file list, enter a file extension in the \field{Filter} field and press \keys{enter}.

		\includegraphics[width=0.9\textwidth]{images/1}
	\item Now that we have our list of files, lets mark the start and end times of our trial.
		Return to the \field{Video} tab and use the navigation keys (see the \field{controls} tab) to find the point in the video when you want to start and stop the tracking.
		Enter these frame numbers in the appropriate fields under the \field{Video} tab.
		The next time you open this file, it will open to the start frame, and when batch processing it will skip to the next video once the end frame is reached.

		\includegraphics[width=0.9\textwidth]{images/2}
	\item Next, we'll define our tracking arenas.
		Each tracking arena will track a single centroid and each arena consists of 4 points for which all video outside of the arena polygon will be ignored.
		Later, the user can use the coordinates of the tracking arena to project the centroid coordinates from the image domain to real world distances.
		To define an arena, press \keys{+} and click 4 corners around the area to track.
		\textbf{Start with the upper left corner}, and continue \textbf{drawing the points in a clockwise direction}.
		A red frame around your video indicates your in \textbf{arena mode}.
		This simply means that your mouse clicks are being tracked to define the arenas.
		Sometimes the mouse clicks do not register correctly (we're working on this!).
		In this case, simply restart the program and it should work again.

		\includegraphics[width=0.9\textwidth]{images/3}
	\item Now go ahead an play your video and see how the default tracking parameters perform.
		While the video is playing (press \keys{P} or the \field{Start} button), you can update the tracking and video parameters in real time.
		When you edit a value and press \keys{enter}, the new settings will be immediately saved to your disk.
		You'll notice some shapes appear on your screen at the tracking is running.
		Detected motion is represented by colored groups of pixels.
		{\color{yellow!50!black}Yellow indicates the largest group of contiguous pixels in the arena}, while {\color{red!80!black} red indicates all other pixels with detected motion} .
		When the largest group of pixels exceeds the threshold you defined, you'll notice a {\color{green!40!black} green circle appear in the cluster of pixels.}
		This is the {\color{green!40!black} tracking centroid} and is the location which is written to disk during the tracking analysis.
		You can use this visual feedback to fine tune the tracking parameters for your specific setup.

		If your need faster performance, try lowering the \field{Scale Video Size} parameter to 0.5 or lower.
		Note that you'll have to redraw your arenas after this, and your new centroids will have different saved locations than the previous ones you tracked as the frame now has new dimensions in pixels.
		Looking to the bottom, you'll see two buffers with progress bars.
		If your \field{Video Load Buffer} is consistently lower than your \field{Frame Tracking Buffer} during tracking playback, this means that you can use more intensive Tracking options (like \field{image cleaning}) without slowing down Animal Trackers overall processing speed.
		To understand the processing pipeline and the video and tracking options, see fig.~\ref{fig:flowchart}.
		To maximize processing speed, you'll want to adjust the settings to that the \field{Frame Tracking Buffer} never empties.

		\includegraphics[width=0.9\textwidth]{images/4}

		\item Now that we've set our start/end frames, arenas, and video/tracking parameters, we can move onto the next video.
			Press \keys{[} and \keys{]} to navigate to the previous or next videos in the list, or you can just use the mouse to select the next video in the \field{Files} tab.
			For each video you want to line up for processing, repeat the steps outlined here.
		\item When your ready to batch process all files, select the first video in the list (batch processing only works from the top to bottom of the \field{Files} list), check the \field{Batch Processing} box, and just start the video playback.
			The batch processing option simply tells Animal Tracker that when it reaches the end of a video.
			Animal Tracker will automatically save the centroid locations to a csv file during playback, so you dont have to worry about saving your tracking results or parameters.
\end{enumerate}

\section{Image Cleaning}

Animal Tracker provides \textit{Open} and \textit{Close} image cleaning operations.
Each of these options requires parameters to be entered for the \textit{erosion} and \textit{dilation} options.
An \textit{open} operation simply consists of an \textit{erosion} operation followed by a \textit{dilation}, and the order is reversed for the \textit{close} option.

The \textit{erosion} and \textit{dilation} operations act on groups of contigious pixels in a binary image.
\textit{erosion} will remove the pixels around the perimiter of each contigous group, while \textit{dilate} will add an extra layer of pixels.
When used together, these operations can be used to join close groups of pixels into a single group (\textit{close}) or seperate a large complex group of pixels into may smaller groups (\textit{open}).
See fig.~\ref{fig:close} for a before and after example of the \textit{close} operation.
Note that these operations can be relatively processor intensive, so only use them if you are willing to foot the cost of increased processing times.

\begin{figure}
	\caption{A before and after comparison of a tracked object with a \textit{close} operation applied to it with \textit{erosion} and \textit{dilation} parameters set to a 30 pixel radius with 3 iterations.
		As seen, the \textit{close} operation transforms close groups of pixels into a single group with more rounded edges.}\label{fig:close}
	\begin{center}
	\includegraphics[width=0.3\textwidth]{images/5}
	\includegraphics[width=0.3\textwidth]{images/7}
	\end{center}
\end{figure}

\begin{figure}
	\caption{A diagram illustrating the underlying structure of animal tracker.
	Arrows indicate the order of operations, while grey bounding rectangles indicate worker threads.
By dividing the jobs across the UI and worker threads, we can optimize the processing times.
Each thread has a frame buffer, so if the processing thread is not ready to take the next frame of video, the video thread will continue extracting and manipulating subsequent frames until its buffer is full.
The processing thread, when ready, will pull its frames from this buffer.
The buffering is necessary to avoid bottlenecks that would be present if we were to process all frames in a strictly sequential manner.}\label{fig:flowchart}

\newcommand{\nodewidth}{19em}
\definecolor{Stroke}{rgb}{0.3,0.3,0.3}
\definecolor{Fill}{rgb}{0.6,0.6,0.6}

\tikzstyle{D} = [rectangle, draw, rounded corners, fill=Goldenrod!20, node distance = 3cm, text width = \nodewidth, anchor = center]
\tikzstyle{C} = [rectangle, draw, rounded corners, fill=green!20, node distance = 3cm, text width = \nodewidth, anchor = center]
\tikzstyle{A} = [rectangle, draw, fill=blue!20, node distance = 3cm, text width = \nodewidth, anchor = center]
\tikzstyle{A} = [rectangle, draw, fill=blue!20, node distance = 3cm, text width = \nodewidth, anchor = center]
\tikzstyle{B} = [rectangle, draw, fill=red!20, node distance = 1.2*\nodewidth, text width = \nodewidth, anchor = center]
\tikzstyle{backgroundBox} = [draw, inner xsep=1em, inner ysep=1em, fill = Fill, draw = Stroke]
\tikzstyle{boxHeader} = [fill=Fill,text = white, right = 10pt, inner ysep = 2pt, inner xsep = 2pt]

\begin{tikzpicture}
	\node [C] (selectFrame) {\textbf{Raw Video}: As soon as Animal Tracker loads a video, it'll start pushing frames down the pipeline until the buffers are full.}; 
	\node [A, below of = selectFrame] (loadFrame) {\textbf{Load Frame}: The video frame is loaded from the disk.};
	\node [A, below of=loadFrame] (scaleVideo) {\textbf{Scale Video}: The video is resized to a fraction of its original dimensions.  This parameter has the greatest effect on reducing processing time.};
	\node [A,below of=scaleVideo] (guassianBlur) {\textbf{Guassian Blur}:  The radius and sd parameters affect the size and roll-off of intensity of the blurring effect.  Its usually good to keep this at 1 or above (odd numbers only), as this will greatly reduce small video compression artefacts.};
	\node [D,below of=guassianBlur] (VideoBuffer) {\textbf{Video Buffer}: Temporary storage of processed frames.};
	\node [B, right of=selectFrame] (MOG2) {\textbf{MOG2}: This controls the adaptive background subtraction.  History determines how may previous frames to use for the background generation and distance refers to the mahanaobis distance when detecting motion (i.e. detection sensitivity).  16 is a good starting value.};
	\node [B, below of=MOG2, node distance = 3.5cm] (DetectionThreshold) {\textbf{Detection Threshold}: The MOG2 algorithm returns a greyscale image where the intensity indicates how much motion there was in a given pixel.  These threshold values will act as the cutoff point to device if a pizel should be classified as detected or not.};
	\node [B, below of=DetectionThreshold, node distance = 4.2cm] (ImageCleaning) {\textbf{Image Cleaning}: Here you can select for open, close, or none.  Open and close use dilation and erosion operations to either reduce or increase connectivity between close groups of pixels.  i.e.\ The close feature can transform a groups of closely separated pixels into a single contiguous group.  This is processor intensive, but can result in much more stable centroids within your target object.};
	\node [B, below of=ImageCleaning, node distance = 3.8cm] (MinimumSize) {\textbf{Minimum Size Filter}: Only contiguous groups of pixels exceeding this size are tracked.  Use this to filter out small detection artefacts.};
	\node [D,below of=MinimumSize, node distance = 2.4cm] (ProcessingBuffer) {\textbf{Processing Buffer}: This buffer holds the frames after the motion tracking has been completed.};
	\node [C, left of=ProcessingBuffer, node distance = 1.2*\nodewidth] (Result) {\textbf{Result}: A csv file containing the pixel locations of all marked centroids from your tracking session.};

\begin{scope}[on background layer]
	\node[backgroundBox, fit=(loadFrame) (guassianBlur)] (videoThread) {};
\end{scope}
\begin{scope}[on background layer]
	\node[backgroundBox, fit=(MOG2) (MinimumSize)] (processingThread) {};
\end{scope}

\node[boxHeader] at (videoThread.north west) {\Large\scshape Video Thread};
\node[boxHeader] at (processingThread.north west) {\Large\scshape Processing Thread};

\draw [-latex'] (selectFrame) -- (loadFrame);
\draw [-latex'] (loadFrame) -- (scaleVideo);
\draw [-latex'] (scaleVideo) -- (guassianBlur);
\draw [-latex'] (guassianBlur) -- (VideoBuffer);
\draw [-latex'] (VideoBuffer)   --++  (11.5em,0) |- (MOG2);
\draw [-latex'] (scaleVideo) -- (guassianBlur);
\draw [-latex'] (MOG2) -- (DetectionThreshold);
\draw [-latex'] (DetectionThreshold) -- (ImageCleaning);
\draw [-latex'] (ImageCleaning) -- (MinimumSize);
\draw [-latex'] (MinimumSize) -- (ProcessingBuffer);
\draw [-latex'] (ProcessingBuffer) -- (Result);

\end{tikzpicture}
\end{figure}

\end{document}
