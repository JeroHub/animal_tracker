# Animal Tracker

Animal Tracker is a MOG2 background subtraction object tracker written in `python3` and is built around the `openCV 3.4.3` and `pyQt5` frameworks for the tracking features and GUI, respectively.
The indented userbase is students with too much video data and not enough time, thus the program is specifically designed to track as fast as possible with minimal attendance from the user.

![Screenshot](Manual/images/4.jpeg)
![demo](Demo.gif)

To get an overview of how Animal Tracker works, see the manual [here](./Manual/Manual.pdf).

## Installation/Dependancies

This program depends on `openCV`.
As `openCV` depends on `ffmpeg` for video codecs, the installation is a bit tricky.
The steps below should guide you through installing opencv (with ffmpeg) on your machine and linking the libraries to `anaconda`, a popular platform for using `python`.

### Ubuntu

On linux, you'll likely have to build your own version of opencv to get ffmpeg integration.
This process can be a bit compilcated if you're unfamiliar with make files and compiling code.
For a detailed walkthrough, you can follow the guide [here](https://rtbecard.gitlab.io/2018/11/03/Installing-opencv-with-ffmpeg-support-in-Anaconda3-(Ubuntu-16.04).html).
No prior coding knowledge is necessary to follow the guide, although it may be usefull if you run into errors during the build process.

### Windows

## License

	Copyright 2018 James Campbell & Jeroen Hubert

    Animal Tracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Animal Tracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Animal Tracker.  If not, see <https://www.gnu.org/licenses/>. 
